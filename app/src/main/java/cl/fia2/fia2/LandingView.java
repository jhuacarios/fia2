package cl.fia2.fia2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LandingView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_view);
        getSupportActionBar().hide();
    }
}
